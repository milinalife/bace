using System.Collections.Generic;

namespace Bace.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
      
        
        public List<Work> Works { get; set; } 
    }
}