﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bace.Models
{
   public class Work
    {
       
        public int WorkId { get; set; }
        public string Summary { get; set; }
        public virtual User User { get; set; }
        public int UserId { get; set; }
    }

    
}
