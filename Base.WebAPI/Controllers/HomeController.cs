﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bace.Models;
using Base.Data;


namespace Base.WebAPI.Controllers
{
    public class HomeController : Controller
    {
        private BaseDbContext _dbContext;
        private readonly IDbSet<User> _people;
        public HomeController()
        {
            _dbContext = new BaseDbContext();
        }
        public HomeController(BaseDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        [HttpGet]
        public ActionResult Index()
        {
            var Users = _dbContext.Users.ToList();
            return View(Users);
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
