﻿using System.Data.Entity.ModelConfiguration;
using Bace.Models;

namespace Base.Data.Configuration
{
    public class TaskConfiguration : EntityTypeConfiguration<Work>
    {
        public TaskConfiguration()
        {
            // User has many Work, Work has one User records
            HasRequired(s => s.User)
               .WithMany(p => p.Works)
               .HasForeignKey(s => s.UserId);
        }
    }
}