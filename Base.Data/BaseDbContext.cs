﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bace.Models;
using Base.Data.Configuration;

namespace Base.Data
{
    
    public class BaseDbContext:DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TaskConfiguration());
            
        }

        public DbSet<User> Users{get; set; }
        public DbSet<Work> Works{get; set;}
        public DbSet<Provider> Providers{get; set; }
        
    }
}
